package ru.t1.vlvov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tm_role")
public class Role {

    @NotNull
    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}
